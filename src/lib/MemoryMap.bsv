/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

*  Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
---------------------------------------------------------------------
*/
package MemoryMap;
	/*=== Project imports ==== */
	import defined_types::*;
	import fast_memory_map::*;
	import slow_memory_map::*;
	`include "slow_instance_defines.bsv"
	`include "fast_instance_defines.bsv"
	`include "instance_defines.bsv"
	`include "core_parameters.bsv"
	/*========================= */


function Tuple2 #(Bool, Bit#(TLog#(Num_Fast_Slaves)))
                    fn_addr_to_slave_num  (Bit#(`PADDR) addr);

    let ft = fn_addr_to_fastslave_num(addr);
    Bool isfast = tpl_1(ft);
    Bit#(TLog#(Num_Fast_Slaves)) x = tpl_2(ft);

    let st = fn_slow_address_mapping(addr);
    Bool isslow = tpl_1(st);
    Bit#(TLog#(Num_Slow_Slaves)) y = tpl_2(st);
    if (isfast)
        return tuple2(isfast, x);
    else if (isslow)
        return tuple2(True,fromInteger(valueOf(SlowPeripheral_slave_num)));
    else
        return tuple2(False,?);

endfunction

function Bool is_IO_Addr(Bit#(`PADDR) addr); // TODO Shuold be PADDR
        if(addr>=`DebugBase && addr<=`DebugEnd)
            return (True);
        `ifdef SDR0MemBase
            else if(addr>=`SDR0MemBase && addr<=`SDR0MemEnd)
            `ifdef FlexBus
                return (True);
            `else
                return (False);
            `endif
        `endif
        `ifdef BOOTROM
            else if(addr>=`BootRomBase && addr<=`BootRomEnd)
                return (False);
        `endif
        `ifdef TCMemory
            else if(addr>=`TCMBase && addr<=`TCMEnd)
                return (False);
        `endif
        else
            return True;
endfunction

endpackage
